![bar](/uploads/4917fd7e724bc7b1e2c2cf0c511601fd/bar.png)

# Slackware libxft-bgra

This repository contains a Slackbuild script and libxft-bgra package for Slackware current, to allow colour emoji in DWM, dmenu and ST.

The script and package were created for my own personal use but please feel free to use them.  However, I make no guarantees that I will maintain this package with each new release of libxft and I am not offering any form of support if you use this.  To be blunt, if the package breaks your system, steals your car and runs off with your wife, you are on your own :-)

The package was built on Slackware current so should only be used on that as it is based on libxft 2.3.4. It also conflicts with Slackware's version of libXft (they use a capital X) so remove that prior to installing the package. 

If you use the Slackbuild rather than the pre-built package, please ensure that the original libXft is still on the system during the compile.

A video is available on youtube showing how I came to create this package:

  https://www.youtube.com/watch?v=IcQslz5Pb5Y

## About libXft and the bgra patch

libXft with BGRA glyph (color emoji) rendering & scaling patches by Maxime Coste. This patched library is a requirement for displaying colour emoji in Suckless software.

libXft is the client side font rendering library, using libfreetype, libX11, and the X Render extension to display anti-aliased text. These libraries should be installed before building libXft. These are already installed on Slackware by default. The original libXft on Slackware should also be on the system if building this libxft-bgra package using the slackbuild script, but should be removed before installation.

The bgra patch was created by Maxime Coste and is available here:

  https://gitlab.freedesktop.org/xorg/lib/libxft/merge_requests/1.patch

Many thanks to Maxime for his great work and also to the team behind libXt. I should also say thanks to the maintainer of the Arch version of this package, syaoran, as I used the instructions in the PKGBUILD to discover how to patch the sources:

  https://aur.archlinux.org/packages/libxft-bgra-git/

All questions regarding libXft should be directed at the
Xorg mailing list:

  https://lists.x.org/mailman/listinfo/xorg

The master development code repository can be found at:

  https://gitlab.freedesktop.org/xorg/lib/libXft


## Who Am I?

My real name is Steve Anelay, but most people just call me OTB. I am the creator of the OldTechBloke Youtube channel ([https://www.youtube.com/c/OldTechBloke](https://www.youtube.com/c/OldTechBloke)). I&#39;m a Linux desktop enthusiast who has been installing and running Linux since 2004. I am definitely not a developer like many on GitLab, just a desktop user with a passion for all things Linux. My videos cover a range of topics, from distro reviews through to commentaries and Linux news items and debates.

## Licence

The content of this repository is licensed under the MIT Licence. Basically, that gives you the right to use, copy, modify, etc. the content how you see fit. You can read the full licence terms here ([https://opensource.org/licenses/MIT](https://opensource.org/licenses/MIT)).



